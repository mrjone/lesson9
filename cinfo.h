#ifndef _CINFO_H_
#define _CINFO_H_

int getWidthConsole();
int getHeightConsole();
void setCursorPosConsole(int _x, int _y);

#endif // _CINFO_H_
