#include <cstdio>
#include <string.h>
#include "cinfo.h"

int main()
{
    char msg[30];
    scanf ("%s", &msg);
    int w, h;
    w = getWidthConsole();
    h = getHeightConsole();
    setCursorPosConsole(w/2 - strlen("Hello world")/2, h/2-1);
    printf("Hello world");
    setCursorPosConsole(w/2 - strlen("My name is")/2, h/2);
    printf("My name is");
    setCursorPosConsole(w/2 - strlen(msg)/2, h/2+1);
    printf("%s", msg);
    return 0;
}
